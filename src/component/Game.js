import React from 'react';
import PropTypes from 'prop-types';

import PlayerInfo from '../model/PlayerInfo';
import BoardGame from './BoardGame';

class Game extends React.Component {
    rand = () => Math.random();

    constructor(props) {
        super(props);
        this.state = {
            player1 : new PlayerInfo(this.props.player1, 1, 0),
            player2 : new PlayerInfo(this.props.player2, 1, 0)
        };
        if(props.rand){
            this.rand = props.rand; // overide rand for testing
        }
    }

    rollDice() {
        return Math.floor(this.rand()*6) + 1;
    }

    rollDices(){
        const player1Dice = this.rollDice();
        const player2Dice = this.rollDice();

        this.setState(state => ({
            player1 : state.player1.update(player1Dice, player1Dice > player2Dice),
            player2 : state.player2.update(player2Dice, player2Dice > player1Dice)
        }));
    }

    render(){
        return (<div className="game">
                    <BoardGame  player1={this.state.player1} player2={this.state.player2} />
                    <div>
                        <div>Let's go!</div>
                        <a class="button" href="#" onClick={() => this.rollDices()}>Roll Dices</a>
                    </div>
                </div>);
    }
}

Game.propTypes = {
    player1: PropTypes.string,
    player2: PropTypes.string,
    rand: PropTypes.func // overide rand for testing
  };

export default Game;
