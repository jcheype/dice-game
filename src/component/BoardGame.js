import React from 'react';
import PropTypes from 'prop-types';
import Player from './Player';
import PlayerInfo  from '../model/PlayerInfo';


const BoardGame = ({player1, player2}) => (
    <div className="board">
        <Player {...player1} />
        <Player {...player2}/>
    </div>
);

BoardGame.propTypes = {
    player1: PropTypes.instanceOf(PlayerInfo),
    player2: PropTypes.instanceOf(PlayerInfo)
};

export default BoardGame;
