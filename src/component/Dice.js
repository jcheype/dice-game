import React from 'react';
import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { 
    faDiceOne,
    faDiceTwo,
    faDiceThree,
    faDiceFour,
    faDiceFive,
    faDiceSix,
} from '@fortawesome/free-solid-svg-icons'

const icons = [
    faDiceOne,
    faDiceTwo,
    faDiceThree,
    faDiceFour,
    faDiceFive,
    faDiceSix,
];

const Dice = ({value}) => <FontAwesomeIcon data-testid="dice-icon" className="dice" icon={icons[value-1]} />;

Dice.propTypes = {
    value: PropTypes.oneOf([1, 2, 3, 4, 5, 6]) 
  };

export default Dice;