import React from 'react';
import { render } from '@testing-library/react';

import Dice from '../Dice';

test('dice 3', () => {
    const dice = render(<Dice value={3} />);
    expect(dice).toMatchSnapshot();
  });

test('invalid dice value should print error console message', () => {
    console.error = jest.fn();
    const dice = render(<Dice value={10} />);
    expect(console.error).toHaveBeenCalled();
});