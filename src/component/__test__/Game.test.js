import React from 'react';
import { shallow, mount, render } from 'enzyme';
import renderer from 'react-test-renderer';

import Game from '../Game';

test('Game component snapshot', () => {
    const game = renderer.create(<Game />).toJSON();
    expect(game).toMatchSnapshot();
  });

test('Game Rolldice value', () => {
  const rand = jest.fn();
  rand
    .mockReturnValueOnce(0.49)
    .mockReturnValueOnce(0.99);
  const game = shallow(<Game player1="foo" player2="bar" rand={rand} />);



  const instance = game.instance();
  // floor(0.49 * 6 + 1) = 3 
  expect(instance.rollDice()).toBe(3);
  
  // floor(0.99 * 6 + 1) = 3 
  expect(instance.rollDice()).toBe(6);
});

test('Game Rolldice value', () => {
  const rand = jest.fn();

  const game = shallow(<Game player1="foo" player2="bar" rand={rand} />);
  const instance = game.instance();

  expect(game.state('player1').diceValue).toBe(1);
  expect(game.state('player1').score).toBe(0);

  expect(game.state('player2').diceValue).toBe(1);
  expect(game.state('player2').score).toBe(0);

  rand
    .mockReturnValueOnce(0.49) //player1
    .mockReturnValueOnce(0.99); //player2
  instance.rollDices();

  // player2 wins
  expect(game.state('player1').diceValue).toBe(3);
  expect(game.state('player1').score).toBe(0);
  expect(game.state('player2').diceValue).toBe(6);
  expect(game.state('player2').score).toBe(1);

  rand
  .mockReturnValueOnce(0.50) //player1
  .mockReturnValueOnce(0.50); //player2
  instance.rollDices();

  // draw
  expect(game.state('player1').diceValue).toBe(4);
  expect(game.state('player1').score).toBe(0);
  expect(game.state('player2').diceValue).toBe(4);
  expect(game.state('player2').score).toBe(1);
});