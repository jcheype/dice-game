import React from 'react';
import { render } from '@testing-library/react';

import BoardGame from '../BoardGame';
import PlayerInfo from '../../model/PlayerInfo';

test('BoardGame component snapshot', () => {
    const boardGame = render(<BoardGame  player1={new PlayerInfo("foo", 3, 2)} player2={new PlayerInfo("bar", 4, 5)} />);
    expect(boardGame).toMatchSnapshot();
});