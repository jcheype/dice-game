import React from 'react';
import { render } from '@testing-library/react';

import Player from '../Player';

test('Player component snapshot', () => {
    const dice = render(<Player diceValue={3} score={10} name="foo" />);
    expect(dice).toMatchSnapshot();
  });