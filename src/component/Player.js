import React from 'react';
import PropTypes from 'prop-types';
import Dice from './Dice';


const Player = ({ name, diceValue, score }) => (
    <div className="player" >
        <div className="name" >{name}</div>
        <Dice value={diceValue} />
        <div className="score" >wins: {score}</div>
    </div>
);

Player.propTypes = {
    name: PropTypes.string,
    diceValue: PropTypes.oneOf([1, 2, 3, 4, 5, 6]),
    score: PropTypes.number
};

export default Player;

