
class PlayerInfo {
    constructor(name, diceValue, score){
        this.name = name;
        this.diceValue = diceValue;
        this.score = score;
        Object.freeze(this);
    }

    update(diceValue, isWin){
        return new PlayerInfo(this.name, diceValue, this.score + (isWin ? 1 : 0))
    }
}

export default PlayerInfo;