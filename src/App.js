import React from 'react';
import './App.css';
import Game  from './component/Game';

function App() {

  return (
    <div className="App">
              <Game player1="You" player2="Computer"/>
    </div>
  );
}

export default App;
